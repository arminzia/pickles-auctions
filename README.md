# Pickles Auctions

This app was created using the latest version of Angular CLI. Simply install the npm packages and run the project. 

```
npm install
ng serve --open
```

# 3rd Party Libraries
To keep things simple I have used two libraries for UI components:
* Bootstrap - https://getbootstrap.com/
* ng-bootstrap - https://ng-bootstrap.github.io/#/home
* Ionicons v4 - https://ionicons.com/v4/

# Project Structure
The code is self-explanatory and straightforward. However I'll explain briefly. 
There's a class definition under the *models* directory, defining the event model. 

The *Home* component displays a data table listing valid events. When the component is initialized (ngOnInit) two methods are called to populate the events and parse the data. 

There's a simple pipe for filtering the list based on the *Name* property. Value matching is case insensitive. 

For the date range, I have used the DatePicker component from NgBootstrap. Simply choose a date range and click the Apply button. The Reset button restores the original data set. 

# Global Event
There's an interface in the `typings.d.ts` file extending the `Window` object with a property named `ngCompRef`. The Home component exposes a public function on this named `addEvents()` which accepts an array of event data. You can invoke this function via the browser's console. Example:

```
window.ngCompRef.addEvents([{ id: 123, name: "nexus", startDate: "3/2/2020", endDate: "3/20/2020", budget: 8000 }])
```

The `addEvents()` function in turn, calls the private `doAddEvents()` function which actually amends the data set.

# Points of Interest
It took me less than 4 hours to complete this project. Obviously, in a production environment I would do things differently. For example: 
* the date range filter could be refactored into a Pipe. So that we get filtered results on the fly. Although it's not a good idea, performance wise. Especially when we have large data sets.
* The Home page could be refactored into its own module, and lazy loaded.
* The date range UI could be refactored into a separate component. Currently, there are a few private functions for handling date range selection and validation, etc.
* The **Status** property on the event model is optional. It could be mandatory and instead of having a string value (active/inactive), it could be a boolean.
* The layout is not 100% responsive. It could be optimized for a mobile-first layout.
* Input validation and error handling are missing from this project. Naturally, in a production environment we'd want proper validation and UI feedback.
* Unit tests are outside of the scope of this assignment. But ideally, we'd want to test all components and business logic in a real-world scenario.