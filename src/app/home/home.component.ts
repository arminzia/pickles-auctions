import { Component, OnInit, NgZone, OnDestroy } from "@angular/core";
import {
  NgbDate,
  NgbCalendar,
  NgbDateParserFormatter
} from "@ng-bootstrap/ng-bootstrap";

import { AppEvent } from "../models/app-event";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit, OnDestroy {
  events: AppEvent[] = [];
  displayedEvents: AppEvent[] = [];
  searchText: string;

  hoveredDate: NgbDate;
  fromDate: NgbDate;
  toDate: NgbDate;

  constructor(
    private ngZone: NgZone,
    private calendar: NgbCalendar,
    public formatter: NgbDateParserFormatter
  ) {
    this.fromDate = calendar.getToday();
    this.toDate = calendar.getNext(calendar.getToday(), "d", 10);
  }

  ngOnInit(): void {
    this.populateEvents();
    this.parseEvents();

    // expose a public function for
    // adding events to the data set.
    window.ngCompRef = window.ngCompRef || {};
    window.ngCompRef.addEvents = this.addEvents.bind(this);
  }

  ngOnDestroy() {
    window.ngCompRef.addEvents = null;
  }

  // public event exposed on window.ngCompRef
  addEvents(events: AppEvent[]) {
    this.ngZone.run(() => this.doAddEvents(events));
  }

  // window.ngCompRef.addEvents() calls
  // this function to actually add events.
  private doAddEvents(events: AppEvent[]) {
    if (!events || !events.length) return;

    events.forEach(value => {
      this.events.push(value);
    });

    this.parseEvents();
  }

  populateEvents() {
    this.events.push({
      id: 1,
      name: "Alex",
      startDate: "9/19/2017",
      endDate: "3/9/2018",
      budget: 88377
    });

    this.events.push({
      id: 2,
      name: "Bob",
      startDate: "11/21/2017",
      endDate: "2/21/2018",
      budget: 608715
    });

    this.events.push({
      id: 3,
      name: "Miboo",
      startDate: "11/1/2017",
      endDate: "6/20/2017",
      budget: 239507
    });

    this.events.push({
      id: 4,
      name: "Trilith",
      startDate: "8/25/2017",
      endDate: "11/30/2017",
      budget: 179838
    });

    this.events.push({
      id: 5,
      name: "Layo",
      startDate: "11/28/2017",
      endDate: "3/10/2018",
      budget: 837850
    });

    this.events.push({
      id: 6,
      name: "Photojam",
      startDate: "7/25/2017",
      endDate: "6/23/2017",
      budget: 858131
    });

    this.events.push({
      id: 7,
      name: "Blogtag",
      startDate: "6/27/2017",
      endDate: "1/15/2018",
      budget: 109078
    });

    this.events.push({
      id: 8,
      name: "Rhyzio",
      startDate: "10/13/2017",
      endDate: "1/25/2018",
      budget: 272552
    });

    this.events.push({
      id: 9,
      name: "Zoomcast",
      startDate: "9/6/2017",
      endDate: "11/10/2017",
      budget: 301919
    });

    this.events.push({
      id: 10,
      name: "Realbridge",
      startDate: "3/5/2018",
      endDate: "10/2/2017",
      budget: 505602
    });

    this.events.push({
      id: 11,
      name: "Armin",
      startDate: "2/29/2020",
      endDate: "3/25/2020",
      budget: 12000
    });
  }

  parseEvents() {
    let events: AppEvent[] = [];
    const today = new Date();

    this.events.forEach(value => {
      const startDate = new Date(value.startDate);
      const endDate = new Date(value.endDate);

      if (startDate <= today && endDate >= today) {
        value.status = "active";
      } else {
        value.status = "inactive";
      }

      // if start-date is greater than end-date,
      // exclude the event.
      if (startDate < endDate) {
        events.push(value);
      }
    });

    this.displayedEvents = events;
  }

  applyDateRange() {
    const fromDate = new Date(
      this.fromDate.year,
      this.fromDate.month - 1,
      this.fromDate.day
    );
    const toDate = new Date(
      this.toDate.year,
      this.toDate.month - 1,
      this.toDate.day
    );

    let events: AppEvent[] = [];

    this.events.forEach(value => {
      const startDate = new Date(value.startDate);
      const endDate = new Date(value.endDate);

      if (startDate >= fromDate && endDate <= toDate) {
        events.push(value);
      }
    });

    this.displayedEvents = events;
  }

  reset() {
    this.displayedEvents = this.events;
    this.parseEvents();
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered(date: NgbDate) {
    return (
      this.fromDate &&
      !this.toDate &&
      this.hoveredDate &&
      date.after(this.fromDate) &&
      date.before(this.hoveredDate)
    );
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return (
      date.equals(this.fromDate) ||
      date.equals(this.toDate) ||
      this.isInside(date) ||
      this.isHovered(date)
    );
  }

  validateInput(currentValue: NgbDate, input: string): NgbDate {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed))
      ? NgbDate.from(parsed)
      : currentValue;
  }
}
