import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { HomeComponent } from "./home/home.component";
import { NameFilterPipe } from "./pipes/name.pipe";

const routes: Routes = [
  { path: "", pathMatch: "full", redirectTo: "home" },
  { path: "home", component: HomeComponent }
];

@NgModule({
  declarations: [HomeComponent, NameFilterPipe],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forRoot(routes, { useHash: true, enableTracing: false }),
    NgbModule
  ],
  exports: [RouterModule, NgbModule]
})
export class AppRoutingModule {}
