export class AppEvent {
  id: number;
  name: string;
  startDate: string;
  endDate: string;
  budget: number;
  status?: string;
}