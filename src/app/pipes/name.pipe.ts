import { Pipe, PipeTransform, Injectable } from "@angular/core";
import { AppEvent } from '../models/app-event';

@Pipe({
  name: "nameFilter",
  pure: false
})
@Injectable()
export class NameFilterPipe implements PipeTransform {
  transform(items: AppEvent[], filter: string): any {
    if (!items) return [];

    if (!filter || filter.length === 0) return items;

    // filter the items array, items which match and return true will be kept,
    // items that don't match will be filtered out.
    return items.filter(item => item.name.toUpperCase().startsWith(filter.toUpperCase()));
  }
}